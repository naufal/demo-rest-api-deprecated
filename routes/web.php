<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'api'], function () use ($app) {
    $app->group(['prefix' => 'client'], function () use ($app) {
        $app->post('register', 'ClientAppController@register');
    });
    $app->group(['prefix' => 'auth', 'middleware' => ['client', 'json_request']], function () use ($app) {
        $app->post('register', 'UserController@register');
        $app->post('login', 'UserController@login');
        $app->post('profile', ['middleware'=> 'auth_token', 'uses' => 'UserController@profile']);
        $app->post('logout', ['middleware'=> 'auth_token', 'uses' => 'UserController@logout']);
    });
    $app->group(['prefix' => 'users', 'middleware' => ['client', 'json_request']], function () use ($app) {
        $app->post('all', 'UserController@all');
    });
});