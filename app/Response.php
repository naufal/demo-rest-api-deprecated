<?php
/**
 * User: littleflower
 * Date: 12/05/17
 * Time: 21:25
 */

namespace App;

use Illuminate\Http\JsonResponse;

class Response
{
    public $status;
    public $message;
    public $data;
    public $errors;

    public static function success($data, $message = 'Success.')
    {
        $response = new Response();
        $response->status = 200;
        $response->message = $message;
        $response->data = $data;
        $response->errors = null;
        return new JsonResponse($response, 200);
    }

    public static function badRequest($errors = 'No details on error.', $message = 'Bad Request.')
    {
        $response = new Response();
        $response->status = 400;
        $response->message = $message;
        $response->data = null;
        $response->errors = $errors;
        return new JsonResponse($response, 400);
    }

    public static function invalidRequest($errors = 'No details on error.', $message = 'Invalid Request.')
    {
        $response = new Response();
        $response->status = 422;
        $response->message = $message;
        $response->data = null;
        $response->errors = $errors;
        return new JsonResponse($response, 422);
    }

    public static function credentialNotFound()
    {
        $response = new Response();
        $response->status = 401;
        $response->message = 'Credential not found.';
        $response->data = null;
        $response->errors = null;
        return new JsonResponse($response, 401);
    }

    public static function tokenProblem($message)
    {
        $response = new Response();
        $response->status = 401;
        $response->message = $message;
        $response->data = null;
        $response->errors = null;
        return new JsonResponse($response, 401);
    }
}