<?php
/**
 * User: littleflower
 * Date: 13/05/17
 * Time: 14:11
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientApp extends Model
{
    protected $fillable = [
        'name', 'detail', 'owner', 'owner_email', 'key'
    ];

    protected $hidden = [
        'password', 'app_id'
    ];
}