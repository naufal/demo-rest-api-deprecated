<?php
/**
 * User: littleflower
 * Date: 13/05/17
 * Time: 14:12
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveLogin extends Model
{
    protected $hidden = [
        'user_id', 'app_id', 'updated_at', 'id'
    ];
}