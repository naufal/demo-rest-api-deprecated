<?php
/**
 * User: littleflower
 * Date: 13/05/17
 * Time: 14:43
 */

namespace App\Http\Middleware;

use App\ClientApp;
use App\Response;
use Closure;
use Illuminate\Http\Request;

class ClientAppMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        if ($request->has('client_key')) {
            try {
                $client = ClientApp::where('key', $request->input('client_key'))->firstOrFail();
            } catch (\Exception $e) {
                return Response::invalidRequest('Invalid client key.');
            }
            $request->request->add(['client_id' => $client->id]);
            return $next($request);
        }
        return Response::invalidRequest('Please provide your client information into the header request.');
    }
}