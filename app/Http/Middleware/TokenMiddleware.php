<?php
/**
 * User: littleflower
 * Date: 15/05/17
 * Time: 11:23
 */

namespace App\Http\Middleware;

use App\ActiveLogin;
use App\Response;
use Closure;
use Illuminate\Http\Request;

class TokenMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        if ($request->has('token')) {
            $active_login = ActiveLogin::where('token', $request->input('token'))->get()->first();
            if ($active_login != null && $active_login->token == $request->token) {
                $request->request->add(['user_id' => $active_login->user_id]);
                $request->request->add(['token_id' => $active_login->id]);
                return $next($request);
            }
            return Response::tokenProblem('Token not found.');
        }
        return Response::tokenProblem('Token not provided.');
    }
}