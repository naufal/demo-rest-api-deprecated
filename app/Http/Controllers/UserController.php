<?php
/**
 * User: littleflower
 * Date: 12/05/17
 * Time: 16:26
 */

namespace App\Http\Controllers;

use App\ActiveLogin;
use App\CollectionResponse;
use App\Response;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required',
            'birthplace' => 'required|max:255',
            'birthday' => 'required|date_format:Y-m-d',
            'gender' => 'required|in:male,female',
            'client_id' => 'required|integer'
        ]);
        $user = new User();
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = sha1($request->password);
        $user->birthplace = $request->birthplace;
        $user->birthday = $request->birthday;
        $user->gender = $request->gender;
        $user->app_id = $request->client_id;
        if ($user->saveOrFail()) {
            return Response::success(null, 'Registration success.');
        }
        return Response::badRequest();
    }

    public function all(Request $request)
    {
        $this->validate($request, [
            'page' => 'required|integer',
            'per_page' => 'required|integer',
            'client_id' => 'required|integer'
        ]);
        $users = User::where('app_id', $request->client_id);
        $count = $users->count();
        $page = $request->page;
        $per_page = $request->per_page;
        $users = $users->get()->forPage($request->page, $request->per_page);
        $response = CollectionResponse::make($count, $page, $per_page, $users);
        return Response::success($response);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|exists:users|max:255',
            'password' => 'required',
            'client_id' => 'required|exists:client_apps,id'
        ]);
        $user = User::where('username', $request->username)->get()->first();
        if (($user->password == sha1($request->password) && ($user->app_id == $request->client_id))) {
            $active_login = new ActiveLogin();
            $active_login->user_id = $user->id;
            $active_login->app_id = $user->app_id;
            $active_login->token = sha1($user->id . date_default_timezone_get());
            try {
                $active_login->saveOrFail();
            } catch (\Exception $e) {
                return Response::badRequest('Token not created', 'Internal service error');
            }
            return Response::success($active_login);
        }
        return Response::credentialNotFound();
    }

    public function profile(Request $request)
    {
        $user = User::where('id', $request->user_id)->get()->first();
        return Response::success($user);
    }

    public function logout(Request $request)
    {
        ActiveLogin::where('id', $request->token_id)->delete();
        return Response::success('Logout success.');
    }
}