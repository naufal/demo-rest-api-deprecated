<?php

namespace App\Http\Controllers;

use App\Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        return Response::invalidRequest($errors);
    }
}
