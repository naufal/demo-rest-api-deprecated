<?php
/**
 * User: littleflower
 * Date: 13/05/17
 * Time: 14:13
 */

namespace App\Http\Controllers;

use App\ClientApp;
use App\Response;
use Illuminate\Http\Request;

class ClientAppController extends Controller
{

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:client_apps',
            'password' => 'required|confirmed',
            'detail' => 'required|max:255',
            'owner' => 'required|max:255',
            'owner_email' => 'required|max:255'
        ]);
        $client = new ClientApp();
        $client->name = $request->name;
        $client->password = sha1($request->password);
        $client->detail = $request->detail;
        $client->owner = $request->owner;
        $client->owner_email = $request->owner_email;
        $client->key = sha1($request->name . ':' . $request->owner);
        if ($client->saveOrFail()) {
            return Response::success($client);
        }
        return Response::badRequest();
    }
}