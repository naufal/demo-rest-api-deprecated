<?php
/**
 * Created by PhpStorm.
 * User: littleflower
 * Date: 13/05/17
 * Time: 16:38
 */

namespace App;

class CollectionResponse
{
    public $count;
    public $page;
    public $per_page;
    public $result = [];

    public static function make($count, $page, $per_page, $data)
    {
        $collection = new CollectionResponse();
        $collection->count = $count;
        $collection->page = $page;
        $collection->per_page = $per_page;
        $collection->result = $data;
        return $collection;
    }
}